var secrets = require('./secrets.json');
var options = {
  host: 'localhost',
  user: secrets.mysqlUser,
  password: secrets.mysqlPw,
  database: process.env.DB=='TEST' ? 'colnoteDB-Test' : 'colnoteDB'
};
if (process.argv[2] == '--production')
  options.port = 3306;
else
  options.port = 8889;
module.exports = options;
