// expose this function to our app using module.exports
module.exports = function(passport) {

  // load all the things we need
  var LocalStrategy   = require('passport-local').Strategy;
  var FacebookStrategy= require('passport-facebook').Strategy;
  var GoogleStrategy  = require('passport-google-oauth').OAuth2Strategy;


// load up the user model
  var User            = require('../db/users.js');
  var configAuth      = require('./auth.js');


  // =========================================================================
  // passport session setup ==================================================
  // =========================================================================
  // required for persistent login sessions
  // passport needs ability to serialize and unserialize users out of session

  // used to serialize the user for the session
  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  // used to deserialize the user
  passport.deserializeUser(function(id, done) {
    User.findOneBy({'id': id}, function(err, user) {
      done(err, user);
    });
  });

  // =========================================================================
  // LOCAL SIGNUP ============================================================
  // =========================================================================
  // we are using named strategies since we have one for login and one for signup
  // by default, if there was no name, it would just be called 'local'

  passport.use('local-signup', new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField : 'email',
    passwordField : 'password',
    passReqToCallback : true // allows us to pass back the entire request to the callback
  },
  function(req, email, password, done) {

    // asynchronous
    // User.findOne wont fire unless data is sent back
    process.nextTick(function() {

    // find a user whose email is the same as the forms email
    // we are checking to see if the user trying to login already exists
    User.findOneBy({'local_email': email}, function (err, user) {
      if (err) return done(err);
      // check to see if theres already a user with that email
      if (user) {
        return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
      } else {
        // if there is no user with that email
        // create the user
        User.insertOne({'local_email': email, 'local_password': User.generateHash(password)}, function (err, newUser) {
          if (err)
            throw err;
          return done(null, newUser);
        });
      }

    });

    });

  }));

  passport.use('local-login', new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField : 'email',
    passwordField : 'password',
    passReqToCallback : true // allows us to pass back the entire request to the callback
  },
  function(req, email, password, done) { // callback with email and password from our form

    // find a user whose email is the same as the forms email
    // we are checking to see if the user trying to login already exists
    User.findOneBy({'local_email': email}, function(err, user) {
      // if there are any errors, return the error before anything else
      if (err)
        return done(err);

      // if no user is found, return the message
      if (!user)
        return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash

      // if the user is found but the password is wrong
      if (!User.validPassword(password, user.local_password))
        return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata

      // all is well, return successful user
      return done(null, user);
    });

  }));

  // =========================================================================
  // FACEBOOK ================================================================
  // =========================================================================
  passport.use(new FacebookStrategy({

    // pull in our app id and secret from our auth.js file
    clientID        : configAuth.facebookAuth.clientID,
    clientSecret    : configAuth.facebookAuth.clientSecret,
    callbackURL     : configAuth.facebookAuth.callbackURL

  },
  // facebook will send back the token and profile
  function(token, refreshToken, profile, done) {

    // asynchronous
    process.nextTick(function() {

      // find the user in the database based on their facebook id
      User.findOneBy({ 'facebook_id' : profile.id }, function(err, user) {

        // if there is an error, stop everything and return that
        // ie an error connecting to the database
        if (err)
            return done(err);

        // if the user is found, then log them in
        if (user) {
            return done(null, user); // user found, return that user
        } else {
          // if there is no user found with that facebook id, create them
          var data = {
            'facebook_id': profile.id,
            'facebook_token': token,
            'facebook_name': profile.displayName
          };
          User.insertOne(data, function (err, newUser) {
            if (err)
              throw err;
            return done(null, newUser);
          });
        }
      });
    });

  }));


  // =========================================================================
  // FACEBOOK ================================================================
  // =========================================================================
  passport.use(new GoogleStrategy({

    clientID        : configAuth.googleAuth.clientID,
    clientSecret    : configAuth.googleAuth.clientSecret,
    callbackURL     : configAuth.googleAuth.callbackURL,

  },
  function(token, refreshToken, profile, done) {

    // make the code asynchronous
    // User.findOne won't fire until we have all our data back from Google
    process.nextTick(function() {

      // try to find the user based on their google id
      User.findOneBy({'google_id': profile.id}, function(err, user) {
        if (err)
            return done(err);

        if (user) {
            // if a user is found, log them in
            return done(null, user);
        } else {
          // if the user isnt in our database, create a new user
          var data = {
            'google_id': profile.id,
            'google_token': token,
            'google_name': profile.displayName,
            'google_email': profile.emails[0].value
          };
          User.insertOne(data, function (err, newUser) {
            if (err)
              throw err;
            return done(null, newUser);
          });
          }
      });
    });

  }));

};
