This file explains the contents of the hidden "secrets.json" file.


//secrets.json
{
  "mysqlUser" : "your mysql username",
  "mysqlPw" : "your mysql password",
  "session" : "secret to use for express session",
  "facebook" : "facebook app secret",
  "google": "google app secret"
}
