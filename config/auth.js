var options = require('./options.js');
var secrets = require('./secrets.json');
module.exports = {
  'facebookAuth': {
    'clientID': '1672633756332372',
    'clientSecret': secrets.facebook,
    'callbackURL': options.domain+'/auth/facebook/callback'
  },
  'googleAuth': {
    'clientID': '472940989027-5ve9v2605jil7ka2kclpqjef5tjb9e5a.apps.googleusercontent.com',
    'clientSecret': secrets.google,
    'callbackURL': options.domain+'/auth/google/callback'
  }
};
