var options;
if (process.argv[2] == '--production')
  options = {domain: 'http://colnote.com', appPort: 9006};
else
  options = {domain: 'http://127.0.0.1:9006', appPort: 9006};
module.exports = options;
