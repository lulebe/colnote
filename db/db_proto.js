var poolPromise = require('./db.js');

function execDB (func, args, cb) {
  poolPromise.then(function (pool) {
    pool.getConnection(function (err, dbConn) {
      if (err) return cb(err);
      func(dbConn, args, function (err, result) {
        dbConn.release();
        cb(err, result);
      });
    });
  });
}

function XXX (dbConn, selector, cb) {
  var sql = "SELECT * FROM ___ WHERE __=?";
  var args = [];
  dbConn.query(sql, args, function (err, result) {
    if (err) return cb(err, null);
    cb(null, result);
  });
};

module.exports = {
  execDB: execDB
};