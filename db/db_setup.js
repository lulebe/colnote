module.exports = function (pool, callback) {

  var Promise = require('promise');
  var fs = require('fs');

  var LATEST_VERSION = 1;
  var version = 0;
  var setups = [setup0];

  pool.getConnection(function (err, conn) {
    if (err) return;
    conn.query("SELECT `value` FROM `internal` WHERE `key`='db_version'", function (err, rows) {
      if (!err && rows.length == 1) version = rows[0].value;
      if (version == LATEST_VERSION) {
        conn.release();
        callback();
      } else 
        setups[version](conn, function () {
          conn.release();
          callback();
        });
    });
  });

  function after (conn, newVersion, cb) {
    version = newVersion;
    conn.query("INSERT INTO internal db_version=? ON DUPLICATE KEY UPDATE db_version=?", [version], function () {
      if (version == LATEST_VERSION) cb();
      else setups[version](conn, cb);
    });
  }



  function setup0 (conn, cb) {
    //TODO init db
    var sql = fs.readFileSync(__dirname + '/db_init.sql', 'utf8');
    conn.query(sql, function (err) {
      if (err) throw err;
      after(conn, LATEST_VERSION, cb);
    });
  }
  
};