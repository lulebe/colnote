module.exports = {
  internal_error:       {msg: 'internal error', code: 500},
  bad_arguments:        {msg: 'bad arguments', code: 400},
  no_name_given:        {msg: 'no name was given', code: 400},
  no_username_given:    {msg: 'no username was given', code: 400},
  no_permission:        {msg: 'No permission for this action', code: 403},
  user_exists:          {msg: 'this user exists already', code: 400},
  user_exists_not:      {msg: 'this user does not exist', code: 404},
  list_exists_not:      {msg: 'this list does not exist', code: 404},
  link_exists_not:      {msg: 'this link does not exist', code: 404},
  media_exists_not:     {msg: 'this medium does not exist', code: 404} 
};
