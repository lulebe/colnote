var poolPromise = require('./db.js');
var errors = require('./db_errors.js');
var crypto = require('crypto');
var io = require('../api/push.js')();

function execDB (func, args, cb) {
  poolPromise.then(function (pool) {
    pool.getConnection(function (err, dbConn) {
      if (err) return cb(err);
      func(dbConn, args, function (err, result) {
        dbConn.release();
        cb(err, result);
      });
    });
  });
}


//args: listid
function getListUsers (dbConn, args, cb) {
  var sql = "SELECT u.id AS id,permissions,username,name FROM list_users lu "
           +"LEFT JOIN users u ON u.id=lu.userid "
           +"WHERE lu.listid=?";
  dbConn.query(sql, [args.listid], function (err, users) {
    if (err) return cb(err);
    cb(null, users);
  });
}

//args: listid,userid
function getListUser (dbConn, args, cb) {
  var listid=args.listid, userid=args.userid;
  var sql = "SELECT * FROM list_users WHERE listid=? AND userid=?";
  dbConn.query(sql, [listid, userid], function (err, lu) {
    if (err) return cb(err, null);
    if (lu.length == 0) return cb(null, {permissions: 0});
    cb(null, lu[0]);
  });
}

//args: listid, adminid, userid, permissions
function changePermissions (dbConn, args, cb) {
  var listid=args.listid, adminid=args.adminid, userid=args.userid, permissions=args.permissions;
  var sql = "INSERT INTO list_users (listid, userid, permissions) VALUES (?,?,?) ON DUPLICATE KEY UPDATE permissions=?";
  getListUser(dbConn, {listid:listid, userid:adminid}, function (err, lu) {
    if (err) return cb(err);
    if(lu.permissions != 3) return cb(errors.no_permission);
    dbConn.query(sql, [listid, userid, permissions, permissions], function (err, result) {
      if (err) return cb(err);
      cb(null);
    });
  });
}



//args: lists (array of lists from lists-table), userlevel (which users should be listed, 1-4)
function getListsContent (dbConn, args, cb) {
  var lists = args.lists;
  var lists_obj = {};
  var list_ids = [];
  lists.forEach(function (list) {
    list.cards = [];
    list.users = [];
    lists_obj[list.id] = list;
    list_ids.push(list.id);
    if (list.tags && list.tags.split)
      list.tags = list.tags.split(',');
    else
      list.tags = [];
  });
  var sql_users = "SELECT u.id,u.name,u.username,lu.listid,lu.permissions "
                 +"FROM list_users lu "
                 +"LEFT JOIN users u ON u.id=lu.userid "
                 +"WHERE lu.listid IN ("+list_ids.join()+") AND lu.permissions>=?";
  var sql_cards = "SELECT c.*,GROUP_CONCAT(t.tag) AS tags "
                 +" FROM cards c "
                 +"LEFT JOIN tags t ON (t.refid=c.id AND t.type=2) "
                 +"WHERE c.listid IN ("+list_ids.join()+") "
                 +"GROUP BY c.id";
  dbConn.query(sql_users, [args.userlevel], function (err, users) {
    if (err) return cb(err);
    users.forEach(function (user) {
      lists_obj[user.listid].users.push(user);
      delete user.listid;
    });
    dbConn.query(sql_cards, [], function (err, cards) {
      if (err) return cb(err);
      if (cards.length == 0) return cb(null, lists);
      var cards_obj = {};
      var card_ids = [];
      cards.forEach(function (card) {
        card.content = [];
        cards_obj[card.id] = card;
        card_ids.push(card.id);
        lists_obj[card.listid].cards.push(card);
        if (card.tags)
          card.tags = cards.tags.split(',');
        else
          card.tags = [];
      });
      var sql_contents = "SELECT * FROM card_content WHERE cardid IN ("+card_ids.join()+")";
      dbConn.query(sql_contents, [], function (err, contents) {
        if (err) return cb(err);
        contents.forEach(function (content) {
          cards_obj[content.cardid].content.push(content);
        });
        cb(null, lists);
      });
    });
  });
}


//args: listid
function getList (dbConn, args, cb) {
  var sql = "SELECT l.*, GROUP_CONCAT(t.tag) AS tags "
           +"FROM lists l "
           +"LEFT JOIN tags t ON (t.refid=l.id AND t.type=1) "
           +"WHERE l.id=? "
           +"GROUP BY l.id";
  dbConn.query(sql, [args.listid], function (err, lists) {
    if (err) return cb(err);
    if (lists.length !== 1) return cb(errors.list_exists_not);
    getListsContent(dbConn, {lists:lists, userlevel:1}, function (err, lists_populated) {
      cb(err, lists_populated[0]);
    });
  });
}


//args: [userid]
function getPublicLists (dbConn, args, cb) {
  var sql_lists = "SELECT l.*,lu.permissions,GROUP_CONCAT(t.tag) AS tags "
                 +"FROM lists l "
                 +"LEFT JOIN list_users lu ON (lu.listid=l.id AND lu.userid=?)"
                 +"LEFT JOIN tags t ON (t.refid=l.id AND t.type=1) "
                 +"WHERE l.public=1 "
                 +"GROUP BY l.id";
  dbConn.query(sql_lists, [args.userid || 0], function (err, lists) {
    if (err) return cb(err);
    if (lists.length == 0) return cb(null, null);
    getListsContent(dbConn, {lists:lists, userlevel:3}, function (err, populated_lists) {
      if (err) return cb(err);
      cb(null, populated_lists);
    });
  });
}

//args: userid
function getListsForUser (dbConn, args, cb) {
  var userid=args.userid;
  var sql_lists = "SELECT l.*,lu.permissions,lu.pinned,lu.unread_changes,GROUP_CONCAT(t.tag) AS tags "
                 +"FROM list_users lu "
                 +"LEFT JOIN lists l ON l.id=lu.listid "
                 +"LEFT JOIN tags t ON (t.refid=l.id AND t.type=1) "
                 +"WHERE lu.userid=? AND lu.permissions>=1 "
                 +"GROUP BY l.id";
  dbConn.query(sql_lists, [userid], function (err, lists) {
    if (err) return cb(err);
    if (lists.length == 0) return cb(null, []);
    getListsContent(dbConn, {lists:lists, userlevel:1}, function (err, populated_lists) {
      if (err) return cb(err);
      cb(null, populated_lists);
    });
  });
}

//either pass userid+listid, viewlink or editlink, set the other two null
//args: listid,userid,viewlink,editlink
function getListWithAccess (dbConn, args, cb) {
  if (args.listid) {
    if (!args.userid)
      args.userid = 0;
    getList(dbConn, {listid:args.listid}, function (err, list) {
      if (err) return cb(err);
      getListUser(dbConn, {listid: args.listid, userid: args.userid}, function (err, lu) {
        if (err) return cb(err);
        if (!list.public && lu.permissions < 1) return cb(errors.no_permission);
        list.permissions = lu.permissions;
        list.unread_changes = lu.unread_changes;
        list.pinned = lu.pinned;
        if (lu.permissions !== 3) {
          delete list.editlink;
          delete list.viewlink;
        }
        cb(null, list);
      });
    });
  } else if (args.viewlink) {
    var sql = "SELECT id FROM lists WHERE viewlink=?";
    dbConn.query(sql, [args.viewlink], function (err, list) {
      if (err) return cb(err);
      if (list.length !== 1) return cb(errors.link_exists_not);
      getList(dbConn, {listid:list[0].id}, function (err, list) {
        if (err) return cb(err);
        list.permissions = 1;
        delete list.editlink;
        cb(null, list);
      });
    });
  } else {
    var sql = "SELECT id FROM lists WHERE editlink=?";
    dbConn.query(sql, [args.editlink], function (err, list) {
      if (err) return cb(err);
      if (list.length !== 1) return cb(errors.link_exists_not);
      getList(dbConn, {listid:list[0].id}, function (err, list) {
        if (err) return cb(err);
        list.permissions = 2;
        cb(null, list);
      });
    });
  }
}



//only admins can get the read link
//args: listid, userid
function getViewLink (dbConn, args, cb) {
  var sql = "SELECT viewlink FROM lists WHERE id=?";
  getListUser(dbConn, args, function (err, lu) {
    if (err) return cb(err);
    if(lu.permissions !== 3) return cb(errors.no_permission);
    dbConn.query(sql, [args.listid], function (err, result) {
      if (err) return cb(err);
      if (result.length !== 1) return cb(errors.list_exists_not, null);
      if (!result[0].viewlink) return cb(errors.link_exists_not, null);
      cb(null, result[0].viewlink);
    });
  });
}

//only admins can get the write link
//args: listid, userid
function getEditLink (dbConn, args, cb) {
  var sql = "SELECT editlink FROM lists WHERE id=?";
  getListUser(dbConn, args, function (err, lu) {
    if (err) return cb(err);
    if(lu.permissions !== 3) return cb(errors.no_permission);
    dbConn.query(sql, [args.listid], function (err, result) {
      if (err) return cb(err);
      if (result.length !== 1) return cb(errors.list_exists_not, null);
      if (!result[0].editlink) return cb(errors.link_exists_not, null);
      cb(null, result[0].editlink);
    });
  });
}

//only admins can change access to a list
//args: listid, userid,
function setViewLink (dbConn, args, cb) {
  var sql = "UPDATE lists SET viewlink=? WHERE id=?";
  getListUser(dbConn, args, function (err, lu) {
    if (err) return cb(err);
    if(lu.permissions != 3) return cb(errors.no_permission);
    var link = createLink();
    dbConn.query(sql, [args.listid, link], function (err) {
      if (err) return cb(err);
      cb(null, link);
    });
  });
}

//only admins can change access to a list
//args: listid, userid
function setEditLink (dbConn, args, cb) {
  var sql = "UPDATE lists SET editlink=? WHERE id=?";
  getListUser(dbConn, args, function (err, lu) {
    if (err) return cb(err);
    if(lu.permissions != 3) return cb(errors.no_permission);
    var link = createLink();
    dbConn.query(sql, [args.listid, link], function (err) {
      if (err) return cb(err);
      cb(null, link);
    });
  });
}

//args: viewlink OR editlink
function getListIdForLink (dbConn, args, cb) {
  if (args.viewlink)
    dbConn.query("SELECT id FROM lists WHERE viewlink=?", [args.viewlink], function (err, lists) {
      if (err) return cb(err);
      if (lists.length !== 1) return cb(errors.link_exists_not);
      cb(null, lists[0].id);
    });
  else if (args.editlink)
    dbConn.query("SELECT id FROM lists WHERE editlink=?", [args.editlink], function (err, lists) {
      if (err) return cb(err);
      if (lists.length !== 1) return cb(errors.link_exists_not);
      cb(null, lists[0].id);
    });
  else
    cb(errors.bad_arguments);
}

//only admins can change access to a list
//args: listid, userid
function setPublic (dbConn, args, cb) {
  var sqlupdate = "UPDATE lists SET public=1 WHERE id=?; UPDATE lists SET viewlink=? WHERE id=? AND viewlink IS NULL;";
  getListUser(dbConn, args, function (err, lu) {
    if (err) return cb(err);
    if (lu.permissions != 3) return cb(errors.no_permission);
    dbConn.query(sqlupdate, [args.listid, createLink(), args.listid], function (err, result) {
      if (err) return cb(err);
      getViewLink(dbConn, args, function (err, viewlink) {
        if (err) return cb(err);
        cb(null, viewlink);
      });
    });
  });
}


//add creating user as admin and potentially added users according to set permissions
//args: [id],userid,name,color,public,viewlink,editlink,users,tags
function saveList (dbConn, args, cb) {
  if (args.users.length < 1) return cb(errors.bad_arguments);
  if (args.listid) updateList(dbConn, args, cb);
  else addList(dbConn, args, cb);
}

function updateList (dbConn, args, cb) {
  getListUser(dbConn, args, function (errLu, lu) {
    if (errLu) return cb(errLu);
    if (lu.permissions < 3) return cb(errors.no_permission);
    //update list
    var sql_list = "UPDATE lists SET name=?,color=?,public=? WHERE id=?;";
    var sql_list_args = [args.name,args.color,args.public==true,args.listid];
    if (args.viewlink) {
      sql_list += "UPDATE lists SET viewlink=? WHERE id=? AND viewlink IS NULL;";
      sql_list_args.push(createLink());
    }
    if (args.editlink) {
      sql_list += "UPDATE lists SET editlink=? WHERE id=? AND editlink IS NULL;";
      sql_list_args.push(createLink());
    }
    dbConn.query(sql_list, sql_list_args, function (errList) {
      if (errList) return cb(errList);
      //update tags
      var sql_tags = "DELETE FROM tags WHERE type=1 AND refid=?;";
      var sql_tags_args = [args.listid];
      args.tags.forEach(function (tag) {
        sql_tags += "INSERT IGNORE INTO tags (tag,refid,type) VALUES (?,?,1);";
        sql_tags_args.push(tag);
        sql_tags_args.push(args.listid);
      });
      dbConn.query(sql_tags, sql_tags_args, function (errTags) {
        if (errTags) return cb(errTags);
        //update users
        var sql_users = "DELETE FROM list_users WHERE listid=?;";
        var sql_users_args = [args.listid];
        sql_users += "INSERT IGNORE INTO list_users (userid,listid,permissions) VALUES (?,?,?);";
        sql_users_args = sql_users_args.concat([args.userid,args.listid,3]);
        args.users.forEach(function (user) {
          if (user.id == args.userid)
            return;
          sql_users += "INSERT IGNORE INTO list_users (userid,listid,permissions) VALUES (?,?,?);";
          sql_users_args.push(user.id);
          sql_users_args.push(args.listid);
          sql_users_args.push(user.permissions);
        });
        dbConn.query(sql_users, sql_users_args, function (errUsers) {
          if (errUsers) return cb(errUsers);
          setUnread(dbConn, {userid:args.userid, listid:args.listid}, function () {
            getListWithAccess(dbConn, {listid: args.listid, userid: args.userid}, cb);
          });
        });
      });
    });
  });
}

function addList (dbConn, args, cb) {
  var sql_list = "INSERT INTO lists (name,color,public,viewlink,editlink) VALUES (?,?,?,?,?)";
  var sql_list_args = [args.name,args.color,args.public ? 1 : 0,
                       args.viewlink ? createLink() : null,
                       args.editlink ? createLink() : null];
  var sql = "";
  var sql_args = [];
  var sql_user = "INSERT IGNORE INTO list_users (userid,listid,permissions) VALUES (?,?,?);";
  var sql_tag = "INSERT IGNORE INTO tags (tag,refid,type) VALUES (?,?,1);";
  dbConn.query(sql_list, sql_list_args, function (err, result) {
    if (err) return cb(err);
    var listid = result.insertId;
    sql += sql_user;
    sql_args = sql_args.concat([args.userid,listid,3]);
    args.users.forEach(function (user) {
      if (user.id == args.userid)
        return;
      sql += sql_user;
      sql_args.push(user.id);
      sql_args.push(listid);
      sql_args.push(user.permissions);
    });
    args.tags.forEach(function (tag) {
      sql += sql_tag;
      sql_args.push(tag);
      sql_args.push(listid);
    });
    dbConn.query(sql, sql_args, function (err) {
      if (err) return cb(err);
      setUnread(dbConn, {userid:args.userid, listid:listid}, function () {
        getListWithAccess(dbConn, {listid: listid, userid: args.userid}, cb);
      });
    });
  });
}

//args:listid,userid
function deleteList (dbConn, args, cb) {
  var lID = args.listid;
  var sql_delete = "DELETE FROM card_content WHERE cardid IN (SELECT id FROM cards WHERE listid=?);";
  sql_delete += "DELETE FROM tags WHERE type=2 AND refid IN (SELECT id FROM cards WHERE listid=?);";
  sql_delete += "DELETE FROM cards WHERE listid=?;";
  sql_delete += "DELETE FROM tags WHERE type=1 AND refid=?;";
  sql_delete += "DELETE FROM lists WHERE id=?;";
  sql_delete += "DELETE FROM list_users WHERE listid=?;";
  getListUser(dbConn, args, function (err, lu) {
    if (err) return cb(err);
    if (lu.permissions !== 3) return cb(errors.no_permission);
    dbConn.query(sql_delete, [lID, lID, lID, lID, lID, lID], function (err) {
      if (err) return cb(err);
      cb(null);
    });
  });
}

//args: userid,listid,pin
function pinList (dbConn, args, cb) {
  var sql = "UPDATE list_users SET pinned=? WHERE userid=? AND listid=?";
  dbConn.query(sql, [args.pin ? 1 : 0, args.userid, args.listid], function (err) {
    if (err) return cb(err);
    getListWithAccess(dbConn, args, cb);
  });
}


//args: userid, listid
function setUnread (dbConn, args, cb) {
  var sql = "UPDATE list_users SET unread_changes=1 WHERE userid!=? AND listid=?";
  dbConn.query(sql, [args.userid, args.listid], function (err) {
    if (err) return cb(err);
    var sql_push = "SELECT userid FROM list_users WHERE listid=?";
    dbConn.query(sql_push, [args.listid], function (err2, users) {
      if (!err2) {
        var userids = [];
        users.forEach(function (user) {
          userids.push(user.userid);
        });
        io.listsChange(userids);
      }
      cb(null);
    });
  });
}
//args: userid, listid
function setRead (dbConn, args, cb) {
  var sql = "UPDATE list_users SET unread_changes=0 WHERE userid=? AND listid=?";
  dbConn.query(sql, [args.userid, args.listid], function (err) {
    if (err) return cb(err);
    cb(null);
  });
}

//args: userid,listid,tags[]
function setListTags (dbConn, args, cb) {
  getListUser(dbConn, args, function (err, lu) {
    if (err) return cb(err);
    if (lu.permissions !== 3) return cb(errors.no_permission);
    var sql = "DELETE FROM tags WHERE refid=? AND type=1;";
    var sql_args = [args.listid];
    args.tags.forEach(function (tag) {
      sql += "INSERT IGNORE INTO tags (tag,refid,type) VALUES (?,?,1);";
      sql_args.push(tag);
      sql_args.push(args.listid);
    });
    dbConn.query(sql, sql_args, function (err) {
      if (err) return cb(err);
      getListWithAccess(dbConn, args, cb);
    });
  });
}

//args: userid, cards[]
function saveCards (dbConn, args, cb) {
  if (!args.cards || args.cards.length == 0) return cb(null);
  saveCardsRecursive(dbConn, args.userid, args.cards, 0, cb);
}
function saveCardsRecursive (dbConn, userid, cards, index, cb) {
  //save card
  if (!cards) return cb(errors.bad_arguments);
  if (cards.length < 1) return cb(errors.bad_arguments);
  var card = cards[index];
  if (!card.title || !card.listid) return cb(errors.bad_arguments);
  if (!card.tags || card.tags.constructor !== Array) return cb(errors.bad_arguments);
  getListUser(dbConn, {userid: userid, listid: card.listid}, function (errUsr, lu) {
    if (errUsr) return cb(errUsr);
    if (lu.permissions < 2) return cb(errors.no_permission);
    var sql_card = "INSERT INTO cards (id,listid,title) VALUES (?,?,?) ON DUPLICATE KEY UPDATE listid=?, title=?";
    dbConn.query(sql_card, [card.id,card.listid,card.title,card.listid,card.title], function (errCard, res) {
      if (errCard) return cb(errCard);
      if (!card.id)
        card.id = res.insertId;
      var sql_tags = "DELETE FROM tags WHERE type=2 AND refid=?;";
      var sql_tags_args = [card.id];
      card.tags.forEach(function (tag) {
        sql_tags += "INSERT IGNORE INTO tags (tag,refid,type) VALUES (?,?,2);";
        sql_tags_args.push(tag);
        sql_tags_args.push(card.id);
      });
      dbConn.query(sql_tags, sql_tags_args, function (errTags) {
        if (errTags) return cb(errTags);
        //TODO add functionality to remove items
        var sql_content_remove_args = [card.id];
        var sql_content_remove_placeholders = [];
        var sql_content = "";
        var sql_content_args = [];
        var sql_content_single = "INSERT INTO card_content "
                                +"(id,cardid,type,content) VALUES (?,?,?,?) "
                                +"ON DUPLICATE KEY UPDATE cardid=?,type=?,content=?;";
        card.content.forEach(function (content) {
          if (!content.id || content.id < 1) content.id='';
          else {
            sql_content_remove_args.push(content.id);
            sql_content_remove_placeholders.push("?");
          }
          var sql_content_arg = [content.id,card.id,content.type,content.content,card.id,content.type,content.content];
          sql_content += sql_content_single;
          sql_content_args = sql_content_args.concat(sql_content_arg);
        });
        var sql_content_remove = "DELETE FROM card_content "
                                +"WHERE cardid=? AND id NOT IN ("+sql_content_remove_placeholders.join(",")+")";
        if (sql_content_remove_placeholders.length == 0)
          sql_content_remove = "DELETE FROM card_content WHERE cardid=?";
        //remove deleted Content
        dbConn.query(sql_content_remove, sql_content_remove_args, function (errRmContent) {
          if (errRmContent) return cb(errRmContent);
          //if no content is added, skip that part
          if (sql_content.length == 0) {
            setUnread(dbConn, {userid: userid, listid: card.listid}, function (errUnread) {
              //don't handle error to keep going
              index++;
              if (index == cards.length) return cb(null);
              saveCardsRecursive(dbConn, userid, cards, index, cb);
            });
          } else
          //update content
          dbConn.query(sql_content, sql_content_args, function (errContent, resContent) {
            if (errContent) return cb(errContent);
            setUnread(dbConn, {userid: userid, listid: card.listid}, function (errUnread) {
              //don't handle error to keep going
              index++;
              if (index == cards.length) return cb(null);
              saveCardsRecursive(dbConn, userid, cards, index, cb);
            });
          });
        });
      });
    });
  });
}

//args: userid, cards[]
function deleteCards (dbConn, args, cb) {
  if (!args.cards || args.cards.length == 0) return cb(null);
  deleteCardsRecursive(dbConn, args.userid, args.cards, 0, cb);
}
function deleteCardsRecursive (dbConn, userid, cards, index, cb) {
  var card = cards[index];
  getListUser(dbConn, {userid: userid, listid: card.listid}, function (errUsr, lu) {
    if (errUsr) return cb(errUsr);
    if (lu.permissions < 2) return cb(errors.no_permission);
    var sql = "DELETE FROM cards WHERE id=?;"
             +"DELETE FROM tags WHERE type=2 and refid=?;"
             +"DELETE FROM card_content WHERE cardid=?";
    dbConn.query(sql, [card.id,card.id,card.id], function (errCard) {
      if (errCard) return cb(errCard);
      setUnread(dbConn, {userid: userid, listid: card.listid}, function (errUnread) {
        //don't handle error to keep going
        index++;
        if (index == cards.length) return cb(null);
        deleteCardsRecursive(dbConn, userid, cards, index, cb);
      });
    });
  });
}

function createLink () {
  var sha1 = crypto.createHash('sha1');
  sha1.update(''+Date.now()+Math.random());
  return sha1.digest('hex');
}

module.exports = {
  getListUsers: function (args, cb) {execDB(getListUsers, args, cb);},
  getListUser: function (args, cb) {execDB(getListUser, args, cb);},
  changePermissions: function (args, cb) {execDB(changePermissions, args, cb);},
  getList: function (args, cb) {execDB(getList, args, cb);},
  getPublicLists: function (args, cb) {execDB(getPublicLists, args, cb);},
  getListWithAccess: function (args, cb) {execDB(getListWithAccess, args, cb);},
  getListsForUser: function (args, cb) {execDB(getListsForUser, args, cb);},
  getViewLink: function (args, cb) {execDB(getViewLink, args, cb);},
  getEditLink: function (args, cb) {execDB(getEditLink, args, cb);},
  setViewLink: function (args, cb) {execDB(setViewLink, args, cb);},
  setEditLink: function (args, cb) {execDB(setEditLink, args, cb);},
  getListIdForLink: function (args, cb) {execDB(getListIdForLink, args, cb);},
  setPublic: function (args, cb) {execDB(setPublic, args, cb);},
  saveList: function (args, cb) {execDB(saveList, args, cb);},
  deleteList: function (args, cb) {execDB(deleteList, args, cb);},
  pinList: function (args, cb) {execDB(pinList, args, cb);},
  setRead: function (args, cb) {execDB(setRead, args, cb);},
  setListTags: function (args, cb) {execDB(setListTags, args, cb);},
  saveCards: function (args, cb) {execDB(saveCards, args, cb);},
  deleteCards: function (args, cb) {execDB(deleteCards, args, cb);}
  };
