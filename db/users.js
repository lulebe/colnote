var bcrypt = require('bcrypt-nodejs');
var poolPromise = require('./db.js');
var errors = require('./db_errors.js');

function execDB (func, args, cb) {
  poolPromise.then(function (pool) {
    pool.getConnection(function (err, dbConn) {
      if (err) return cb(err);
      func(dbConn, args, function (err, result) {
        dbConn.release();
        cb(err, result);
      });
    });
  });
}

function findOneBy (dbConn, selector, cb) {
  var sql = "SELECT * FROM users WHERE ";
  var args = [];
  for (var i=0;i<Object.keys(selector).length;i++) {
    sql += i>0 ? "AND " : "";
    sql += Object.keys(selector)[i]+"=? ";
    args.push(selector[Object.keys(selector)[i]]);
  }
  sql += "LIMIT 0,1";
  dbConn.query(sql, args, function (err, rows) {
    if (err) return cb(err, null);
    if (rows.length == 0) return cb(null, null);
    cb(null, rows[0]);
  });
}

function findAllBy (dbConn, selector, cb) {
  var sql = "SELECT * FROM users WHERE ";
  var args = [];
  for (var i=0;i<Object.keys(selector).length;i++) {
    sql += i>0 ? "AND " : "";
    sql += Object.keys(selector)[i]+"=? ";
    args.push(selector[Object.keys(selector)[i]]);
  }
  dbConn.query(sql, args, function (err, rows) {
    cb(err, rows);
  });
}

function insertOne (dbConn, key_values, cb) {
  var sql = "INSERT INTO users (";
  var args = [];
  var qmarks = "";
  for (var i=0;i<Object.keys(key_values).length;i++) {
    sql += i>0 ? "," : "";
    qmarks += i>0 ? ",?" : "?";
    sql += Object.keys(key_values)[i];
    args.push(key_values[Object.keys(key_values)[i]]);
  }
  sql += ") VALUES ("+qmarks+")";
  dbConn.query(sql, args, function (err, result) {
    if (err) return cb(err, null);
    //get inserted user
    dbConn.query("SELECT * FROM users WHERE id=?", [result.insertId], function (err2, rows) {
      if (err2) return cb(null, null);
      return cb (null, rows[0]);
    });
  });
}

function isComplete (dbConn, id, cb) {
  findOneBy(dbConn, {"id": id}, function (err, user) {
    if (err) return cb(err);
    if (user.name && user.username) return cb(null, true);
    cb(null, false);
  });
}

function complete (dbConn, args, cb) {
  args.name = args.name.trim();
  args.username = args.username.trim();
  if (!args.name) return cb(errors.no_name_given);
  if (!args.username) return cb(errors.no_username_given);
  var sql = "UPDATE users SET name=?, username=? WHERE id="+args.id;
  dbConn.query(sql, [args.name, args.username], function (err) {
    if (err && err.code == 'ER_DUP_ENTRY') return cb(errors.user_exists);
    cb(err);
  });
}

//args: userid,self
function getSafe (dbConn, args, cb) {
  var rows;
  if (args.self)
    rows = "id,username,name,local_email,facebook_name,facebook_email,google_name,google_email";
  else
    rows = "id,username,name";
  dbConn.query("SELECT "+rows+" FROM users WHERE id=?", [args.userid], function (err, users) {
    if (err) return cb(err);
    if (users.length !== 1) return cb(null, null);
    cb(null, users[0]);
  });
}

//args: partial username/name AS searchquery
function search (dbConn, args, cb) {
  var sql = "SELECT id,username,name FROM users WHERE username LIKE CONCAT(?, '%') OR name LIKE CONCAT(?, '%')";
  dbConn.query(sql, [args.searchquery,args.searchquery], cb);
}

//args: id + every field to change, for local_password:
//local_password + local_password_old
function change (dbConn, args, cb) {
  dbConn.query("SELECT * FROM users WHERE id=?", [args.id], function (err, user) {
    if (err) return cb(err);
    if (user.length !== 1) return cb(errors.user_exists_not);
    user = user[0];
    var updates = [];
    var dbArgs = [];
    if (args.name) {
      updates.push("name=?");
      dbArgs.push(args.name);
    }
    if (args.username) {
      updates.push("username=?");
      dbArgs.push(args.username);
    }
    if (args.local_email) {
      updates.push("local_email=?");
      dbArgs.push(args.local_email);
    }
    if (args.local_password && validPassword(args.local_password_old, user.local_password)) {
      updates.push("local_password=?");
      dbArgs.push(generateHash(args.local_password));
    }
    var sql = "UPDATE users SET " + updates.join() + " WHERE id=?";
    dbArgs.push(args.id);
    dbConn.query(sql, dbArgs, function (err) {
      if (err) return cb(err);
      getSafe(dbConn, {userid: args.id, self: true}, function (err, user_new) {
        if (err) return cb(err);
        cb(null, user_new[0]);
      });
    });
  });
}

function generateHash (password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
}
function validPassword (password, hash) {
  return bcrypt.compareSync(password, hash);
}



module.exports = {
  generateHash: generateHash,
  validPassword: validPassword,
  findOneBy: function (args, cb) {execDB(findOneBy, args, cb);},
  insertOne: function (args, cb) {execDB(insertOne, args, cb);},
  isComplete: function (userid, cb) {execDB(isComplete, userid, cb);},
  complete: function (args, cb) {execDB(complete, args, cb);},
  getSafe: function (args, cb) {execDB(getSafe, args, cb);},
  search: function (args, cb) {execDB(search, args, cb);},
  change: function (args, cb) {execDB(change, args, cb);}
};
