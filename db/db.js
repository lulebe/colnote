var mysql = require('mysql');
var Promise = require('promise');
var options = require('../config/db.js');
var setup = require('./db_setup.js');

//connect mysql + update
var promise = new Promise(function (resolve, reject) {
  var pool = mysql.createPool({
    connectionLimit : 10, //important
    host: options.host,
    port: options.port,
    user: options.user,
    password: options.password,
    database: options.database,
    debug: false,
    multipleStatements: true
  });
  setup(pool, function () {
    resolve(pool);
  });
});


module.exports = promise;
