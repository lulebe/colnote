module.exports = function (app, passport) {
  var User = require('../db/users.js');
  app.get('/', function (req,res) {
    if (req.isAuthenticated())
      res.redirect('/app');
    else
      res.render('index.hbs');
  });
  app.get('/login', function (req, res) {
    res.render('login.hbs', {message: req.flash('loginMessage')});
  });
  app.post('/login', passport.authenticate('local-login', {
    successRedirect : '/app',
    failureRedirect : '/login',
    failureFlash : true
  }));
  app.get('/signup', function (req, res) {
    res.render('signup.hbs', {message: req.flash('signupMessage')});
  });
  app.post('/signup', passport.authenticate('local-signup', {
    successRedirect : '/setup',
    failureRedirect : '/signup',
    failureFlash : true
  }));
  app.get('/profile', isLoggedIn, function(req, res) {
    res.render('profile.hbs', {user : req.user});
  });
  app.get('/logout', function (req, res) {
    req.logout();
    req.session.destroy();
    res.redirect('/');
  });
  app.get('/auth/facebook', passport.authenticate('facebook', {scope: 'email'}));
  app.get('/auth/facebook/callback', passport.authenticate('facebook', {
    successRedirect: '/setup',
    failureRedirect: '/'
  }));
  app.get('/auth/google', passport.authenticate('google', {scope: ['profile', 'email']}));
  app.get('/auth/google/callback', passport.authenticate('google', {
    successRedirect: '/setup',
    failureRedirect: '/'
  }));

  app.get('/setup', isLoggedIn, function (req, res) {
    //fill in standard fields for user
    User.isComplete(req.user.id, function (err, complete) {
      if (complete) {
        res.redirect('/app');
      } else {
        var render_args = {
          user: req.user,
          name: req.user.facebook_name || req.user.google_name || ''
        };
        res.render('setup.hbs', render_args);
      }
    });
  });
  app.post('/setup', isLoggedIn, function (req, res) {
    var sql_args = {
      id: req.user.id,
      name: req.body.name,
      username: req.body.username
    };
    var render_args = {
      user: req.user,
      name: req.user.facebook_name || req.user.google_name || '',
      username: ''
    };
    User.complete(sql_args, function (err) {
      if (err) req.flash('setuperror', err.msg);
      if (err) {
        render_args.message = req.flash('setuperror');
        res.render('setup.hbs', render_args);
        return;
      }
      User.isComplete(req.user.id, function (err, complete) {
        console.log(err);
        req.flash('setuperror', err);
        if (complete) {
          res.redirect('/app');
        } else {
          render_args.message = req.flash('setuperror');
          res.render('setup.hbs', render_args);
        }
      });
    });
  });

  app.get('/app', isLoggedIn, function (req, res) {
    User.isComplete(req.user.id, function (err, complete) {
      if (err || !complete) return res.redirect('/setup');
      res.sendFile(__dirname + '/static/dist/index.html');
    });
  });

  function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
      next();
    else
      res.redirect('/');
  }
};
