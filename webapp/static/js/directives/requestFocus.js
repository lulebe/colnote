angular.module('lulebe.Helpers', []).directive('requestFocus', function ($timeout) {
  return {
    restrict: 'A',
    link: function (scope, elem, attrs) {
      elem[0].focus();
      scope.$watch(attrs.requestFocus, function (val) {
        if (val) {
          $timeout(function () {elem[0].focus();});
          console.log(12);
        }
      });
    }
  }
});