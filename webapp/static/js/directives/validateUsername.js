require('services/user.js');

angular.module('colnote').directive('validateUsername', function (user, $q) {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$asyncValidators.username = function(modelValue, viewValue) {
        return $q(function (resolve, reject) {
          user.search(viewValue).then(function (foundUsers) {
            if (foundUsers.length == 1) resolve();
            reject();
          });
        });
      };
    }
  };
});