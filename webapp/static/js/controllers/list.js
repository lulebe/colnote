require('services/lists.js');
require('services/user.js');
require('services/userSelf.js');
require('controllers/listcarddialog.js');

angular.module('colnote').controller('ListCtrl', function ($stateParams, $state, $rootScope, Lists, user, userSelf, $mdDialog, $mdMedia) {

  var vm = this;

  var init = true;

  vm.openCard = function (cardid, action) {
    $state.go('list', {listid:vm.list.id, cardid:cardid});
  }

  var openCard = function (cardid, action) {
    if (cardid != 0)
      var card = vm.list.cards.findById(cardid);
    else
      var card = vm.list.cards[vm.list.cards.length-1];
    $mdDialog.show({
      fullscreen: ($mdMedia('xs') || $mdMedia('sm')),
      templateUrl: '/partials/dialogs/list_card.html',
      controller: 'ListCardDialogCtrl',
      escapeToClose: false,
      locals: {
        card: card,
        action: action,
        permissions: vm.list.permissions
      }
    });
  };

  var listener = function (myLists) {
    vm.list = myLists.findById($stateParams.id);
    if (!vm.list) {
      $state.go('dashboard');
      return;
    }
    if (init && $stateParams.cardid) {
      openCard($stateParams.cardid, $stateParams.action || 'open');
    } else if (init && $stateParams.addcard)
      addCard(null);
    init = false;
    vm.list.unread_changes = 0;
    Lists.setRead(vm.list.id);
  };
  Lists.onMyListsChange(listener);
  $rootScope.$on('$stateChangeStart', function (ev, toState) {
    if (toState.name != 'list')
      Lists.removeOnMyListsChange(listener);
  });

  vm.addCard = function () {
    Lists.createCard(vm.list.id).then(function (card) {
      $state.go('list', {listid:vm.list.id,cardid:0});
    });
  };

  vm.deleteList = function () {
    Lists.deleteList(vm.list.id).then(function () {
      $state.go('dashboard');
    });
  }

});
