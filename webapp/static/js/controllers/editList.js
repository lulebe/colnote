require('services/user.js');
require('services/userSelf.js');
require('services/Lists.js');
require('directives/validateUsername.js');
require('components/hashtagInput.js');

angular.module('colnote').controller('EditlistCtrl', function ($scope, $timeout, $state, $stateParams, user, Lists, userSelf) {
  
  var vm = this;
  
  
  vm.colors = [
    {short: '444', long: '444444'},
    {short: '006', long: '000066'},
    {short: '04A', long: '0044AA'},
    {short: '060', long: '006600'},
    {short: '090', long: '009900'},
    {short: '603', long: '660033'},
    {short: 'A0A', long: 'AA00AA'},
    {short: 'C00', long: 'CC0000'},
    {short: 'E05', long: 'EE0055'},
    {short: 'EA0', long: 'EEAA00'}
  ];
  
  if ($stateParams.id) {
    Lists.getList($stateParams.id).then (function (list) {
      vm.list = list;
      setColorIndex();
    })
  } else {
    userSelf.user.then(function (user) {
      vm.list = {
        name: "",
        color: vm.colors[0].long,
        tags: [],
        users: [{id: user.id, name: user.name, username: user.username, permissions: 3}],
        public: false,
        viewlink: false,
        editlink: false
      };
      setColorIndex();
    });
  }
  
  function setColorIndex () {
    vm.colors.some(function (color,index) {
      if (color.long == vm.list.color) {
        vm.chosencolor = index;
        return true;
      }
      return false;
    });
  }
  $scope.$watch(function () {return vm.chosencolor;}, function (index) {
    vm.list.color = vm.colors[index].long;
  });
  
  vm.editviewlinkdisabled = false;
  
  vm.adduser_loading = false;
  vm.addname = "";
  
  var viewlinksavedstate = false;
  $scope.$watch(function () {return vm.list.public;}, function (public) {
    if (public) {
      viewlinksavedstate = vm.list.viewlink;
      vm.list.viewlink = true;
    } else {
      vm.list.viewlink = viewlinksavedstate;
    }
    vm.editviewlinkdisabled = vm.list.public;
  });
  
  vm.adduser = function (permissions) {
    vm.addname = vm.addname.trim();
    if (vm.addname == '') return;
    vm.adduser_loading = true;
    user.search(vm.addname).then(function (foundUsers) {
      if (foundUsers.length == 1) {
        vm.adduser_loading=false;
        var usr = foundUsers[0];
        usr.permissions = permissions;
        vm.list.users.push(usr);
        vm.addname="";
      } else {
        //TODO tell that user was not found
        vm.adduser_loading = false;
        return;
      }
    });
    
  };
  vm.removeuser = function (index) {
    vm.list.users.splice(index, 1);
  };
  vm.searchUsers = function (query) {
    return user.search(query);
  }
  
  vm.save = function () {
    Lists.saveList(vm.list).then(function(list) {
      $state.go('list', {id:list.id});
    });
  }
  
  
});