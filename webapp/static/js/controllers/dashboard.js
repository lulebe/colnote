/* jslint browser:true */
/* global angular */
/* global require */

require('services/lists.js');
require('components/listCard.js');
require('components/listCardBig.js');

angular.module('colnote').controller('DashboardCtrl', function (Lists, $rootScope) {
  'use strict';
  var vm = this;
  
  Lists.onMyListsChange(function (myLists) {
    vm.lists = myLists;
    vm.lists_pinned = [];
    myLists.forEach(function (list) {
      if (list.pinned) vm.lists_pinned.push(list);
    });
  });
});
