require('services/lists.js');
require('components/listCard.js');
require('components/card.js');

angular.module('colnote').controller('SearchCtrl', function ($stateParams, Lists) {
  
  var vm = this;
  
  vm.query = $stateParams.query;
  
  Lists.search($stateParams.query).then(function (results) {
    vm.results = results;
  });
});