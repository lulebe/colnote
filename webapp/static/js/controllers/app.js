require('services/userSelf.js');
require('services/lists.js');

angular.module('colnote')
.controller('AppCtrl', function ($rootScope, $mdSidenav, $mdDialog, ssSideNav, $translate, $state, userSelf, Lists) {
  var vm = this;

  vm.navOpen = false;
  $rootScope.$on('$stateChangeStart', function () {
    $mdDialog.hide();
  });
  $rootScope.$on('$stateChangeSuccess', function (event, toState) {
    vm.title = toState.data.titleKey;
  });
  vm.menu = ssSideNav;
  function updateNav (myLists) {
    var menuLists = [];
    myLists.forEach(function (list) {
      menuLists.push({
        name: list.name,
        type: 'link',
        icon: list.unread_changes ? 'mdi mdi-information-outline' : 'mdi mdi-read',
        state: 'list({id:' + list.id + '})'
      });
    });
    ssSideNav.sections[1].children = [ssSideNav.sections[1].children[0]].concat(menuLists);
  }
  Lists.onMyListsChange(updateNav);

  userSelf.user.then(function (usr) {vm.user=usr;});

  vm.searchQuery = "";
  vm.search = function () {
    if (vm.searchQuery.length >= 3) {
      //TODO implement search
      $state.go('search', {query: vm.searchQuery});
      vm.searchQuery = "";
    }
  }
});
