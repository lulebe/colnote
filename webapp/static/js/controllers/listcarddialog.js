require('directives/requestFocus.js');

angular.module('colnote').controller('ListCardDialogCtrl', function ($scope, $mdDialog, $state, Lists, card, action, permissions) {
  $scope.card = card;
  $scope.permissions = permissions;
  $scope.unsavedChanges = false;
  $scope.close = function () {
    Lists.getList(card.listid, true); //revert changes
    close();
  };
  $scope.save = function () {
    Lists.saveCards([$scope.card]).finally(function () {
      close();
    });
  };
  $scope.delete = function () {
    Lists.deleteCard($scope.card).finally(function () {
      close();
    });
  };
  $scope.addItem = function () {
    var item = {type:0, cardid: card.id, content: ""};
    $scope.card.content.push(item);
    item.editing = true;
    $scope.unsavedChanges = true;
  };
  $scope.saveAddedItem = function () {
    $scope.card.content.push({type: 0, cardid: card.id, content: $scope.adding.text});
    $scope.adding.text = "";
    $scope.adding.active = false;
    $scope.unsavedChanges = true;
  };
  $scope.edit = function (item) {
    if (permissions>=2) item.editing = true;
    $scope.unsavedChanges = true;
  }
  $scope.deleteItem = function (index) {
    $scope.card.content.splice(index,1);
    $scope.unsavedChanges = true;
  };
  if (action == 'add_item') {
    $scope.addItem();
  }
  function close () {
    $mdDialog.hide();
    $state.go('list', {listid: card.listid, cardid: null});
  }
});