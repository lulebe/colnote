//include libs
require('libs/angular-socket.js');
require('libs/angular-material-sidenav.js');
require('libs/angular-mosaic.js');
require('libs/angular-material-datetimepicker.js');
require('libs/angular-youtube-embed.js');

require('directives/requestFocus.js');

var deps = [
  'ngMaterial',
  'ngMessages',
  'ngAnimate',
  'ngCookies',
  'ui.router',
  'btford.socket-io',
  'pascalprecht.translate',
  'sasrio.angular-material-sidenav',
  'codinghitchhiker.mosaic',
  'ngMaterialDatePicker',
  'youtube-embed',
  'colnotePartials',
  'lulebe.Helpers'
];
var colnote = angular.module('colnote', deps);

//include required files
require('controllers/app.js');
require('controllers/dashboard.js');
require('controllers/editList.js');
require('controllers/users.js');
require('controllers/list.js');
require('controllers/search.js');

//config material design colors
colnote.config(function ($mdThemingProvider) {
  $mdThemingProvider.theme('default')
  .primaryPalette('teal')
  .accentPalette('amber')
  ;//.dark();
});

//config ui-router
colnote.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/dashboard");
  $stateProvider
    .state('dashboard', {
      url: '/dashboard',
      templateUrl: '/partials/dashboard.html',
      controller: 'DashboardCtrl',
      controllerAs: 'dash',
      data: {
        titleKey: 'TITLE_DASHBOARD'
      }
    })
    .state('list', {
      url: '/list/{id:int}?cardid&addcard&action',
      templateUrl: '/partials/list.html',
      controller: 'ListCtrl',
      controllerAs: 'list',
      data: {
        titleKey: 'TITLE_LIST'
      }
    })
    .state('editlist', {
      url: '/editlist/:id',
      templateUrl: '/partials/editlist.html',
      controller: 'EditlistCtrl',
      controllerAs: 'editlist',
      data: {
        titleKey: 'TITLE_EDITLIST'
      }
    })
    .state('users', {
      url: '/users',
      templateUrl: '/partials/users.html',
      controller: 'UsersCtrl',
      controllerAs: 'users',
      data: {
          titleKey: 'TITLE_USERS'
      }
    })
    .state('search', {
      url: '/search?query',
      templateUrl: '/partials/search.html',
      controller: 'SearchCtrl',
      controllerAs: 'search',
      data: {
        titleKey: 'TITLE_SEARCH'
      }
    })
    .state('settings', {
      url: '/settings',
      templateUrl: '/partials/settings.html',
      controller: 'SettingsCtrl',
      controllerAs: 'settings',
      data: {
        titleKey: 'TITLE_SETTINGS'
      }
    });
});

//config translations
colnote.config(function ($translateProvider) {
  // configures staticFilesLoader
  $translateProvider.useStaticFilesLoader({
    prefix: '/res/angular-translations/locale-',
    suffix: '.json'
  });
  $translateProvider.registerAvailableLanguageKeys(['en', 'de'], {
    'en_*': 'en',
    'de_*': 'de'
  });
  $translateProvider.determinePreferredLanguage();
  $translateProvider.fallbackLanguage('en');
  $translateProvider.useSanitizeValueStrategy('escape');
});

//config sidenav
colnote.config(function (ssSideNavSectionsProvider, $mdThemingProvider) {
  //to use correctly, replace all "{{section.name}}" inside the "angular-material-sidenav.js" with "{{section.name | tranlate}}"
  ssSideNavSectionsProvider.initWithSections([
    {
      id: 'menu_dashboard',
      name: 'TITLE_DASHBOARD',
      type: 'link',
      state: 'dashboard',
      icon: 'mdi mdi-view-dashboard'
    },
    {
      id: 'menu_lists',
      name: 'TITLE_LISTS',
      type: 'heading',
      icon: 'mdi mdi-view-list',
      children: [{
        name: 'TITLE_LIST_NEW',
        type: 'link',
        state: 'editlist',
        icon: 'mdi mdi-plus-box'
      }]
    },
    {
      id: 'menu_users',
      name: 'TITLE_USERS',
      type: 'link',
      state: 'users',
      icon: 'mdi mdi-account-multiple'
    },
    {
      id: 'menu_settings',
      name: 'TITLE_SETTINGS',
      type: 'link',
      state: 'settings',
      icon: 'mdi mdi-settings'
    }
  ]);
  ssSideNavSectionsProvider.initWithTheme($mdThemingProvider);
});

colnote.factory('authInterceptor', function ($window) {
  return {
    responseError: function (res) {
      if (res.status === 401) {
        $window.location = '/login';
        return $q.reject(res);
      }
    }
  };
});
colnote.config(function ($httpProvider) {
  $httpProvider.interceptors.push('authInterceptor');
});


if (!Array.prototype.findById)
  Array.prototype.findById = function (id) {
    var ret = false;
    this.some(function (val) {
      if (val.id==id) {
        ret = val;
        return true;
      }
      return false;
    });
    return ret;
  }
