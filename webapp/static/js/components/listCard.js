require('services/lists.js');

angular.module('colnote').component('listCard', {
  bindings: {
    list: '=',
    showActions: '='
  },
  templateUrl: '/partials/components/listcard.html',
  controller: function (Lists) {
    this.pin = function () {
      Lists.pin(this.list.id);
    };
    this.unpin = function () {
      Lists.unpin(this.list.id);
    };
    this.delete = function () {
      Lists.deleteList(this.list.id);
    };
  }
});