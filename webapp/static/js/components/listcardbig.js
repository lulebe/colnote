require('services/lists.js');

angular.module('colnote').component('listCardBig', {
  bindings: {
    list: '=',
    showActions: '='
  },
  templateUrl: '/partials/components/listcardbig.html',
  controller: function (Lists) {
    this.unpin = function () {
      Lists.unpin(this.list.id);
    };
    this.delete = function () {
      Lists.deleteList(this.list.id);
    };
  }
});