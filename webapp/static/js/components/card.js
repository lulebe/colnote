require('services/lists.js');

angular.module('colnote').component('card', {
  bindings: {
    card: '=',
    permissions: '=',
    showActions: '=',
    open: '&'
  },
  templateUrl: '/partials/components/card.html',
  controller: function (Lists) {
    this.delete = function () {
      Lists.deleteCard(this.card);
    };
  }
});