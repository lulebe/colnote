angular.module('colnote').component('hashtagInput', {
  bindings: {
    tags: '='
  },
  templateUrl: '/partials/components/hashtaginput.html',
  controller: function () {
    var vm = this;
    vm.input = '';
    if (Object.prototype.toString.call(vm.tags) !== '[object Array]')
      vm.tags = [];
    
    var tagRegex = new RegExp("^\\w+[\\s,]$");
    vm.change = function () {
      if(vm.input.match(tagRegex)) {
        vm.tags.push(vm.input.trim().replace('#','').replace(',',''));
        vm.input = '';
      }
    };
    vm.remove = function (index) {
      vm.tags.splice(index, 1);
    }
  }
});