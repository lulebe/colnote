require('services/userSelf.js');

angular.module('colnote').factory('user', function ($q, $http, userSelf) {
  
  var testuser = {
    id: 12,
    name: "Test User",
    username: "testuser"
  };
  
  function search(query) {
    return $http.get('/api/users', {params: {q: query}}).then(function (res) {
      return res.data;
    }, function (res) {
      return [];
    });
  }
  
  function sharedListsWith () {
    return $q.when([
      {}
    ]);
  }
  
  return {
    search: search,
    sharedListsWith: sharedListsWith
  }
});