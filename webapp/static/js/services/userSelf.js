angular.module('colnote').factory('userSelf', function ($q, $http) {
  var user = $http.get('/api/users').then(function (res) {
    return res.data;
  });
  return {
    user: user
  };
});