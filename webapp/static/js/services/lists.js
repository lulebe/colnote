require('services/userSelf.js');
require('services/socket.js');

angular.module('colnote').factory('Lists', function ($q, $http, $translate, userSelf, Socket, $mdDialog) {

  var myLists = [];
  var myLists_obj = {};
  var firstLoad = true;
  var firstLoadDeferred = $q.defer();


  var listeners = [];
  function onMyListsChange (listener) {
    listeners.push(listener);
    if (!firstLoad)
      listener(myLists);
  }
  function removeOnMyListsChange (listener) {
    listeners.splice(listeners.indexOf(listener),1);
  }
  function myListsChange () {
    listeners.forEach(function (listener) {
      listener(myLists);
    });
  }
  Socket.on('listsChange', function () {
    getAllLists(true);
  });

  function getAllLists (forceReload) {
    if (myLists.length == 0 || forceReload)
      return $http.get('/api/lists', {params: {mylists: true}}).then(function (res) {
        myLists = res.data;
        myLists_obj = {};
        myLists.forEach(function (list) {
          myLists_obj[list.id] = list;
        });
        if (firstLoad) {
          firstLoad = false;
          firstLoadDeferred.resolve();
        }
        myListsChange();
        return res.data;
      });
    return $q.when(myLists);
  }

  function getList (id, forceLoad) {
    var deferred = $q.defer();
    firstLoadDeferred.promise.then(function () {
      if (!forceLoad && myLists_obj[id]) {
        return deferred.resolve(myLists_obj[id]);
      }
      return $http.get('/api/lists/'+id).then(function (res) {
        if (!res.data)
          deferred.reject();
        if (myLists_obj[id])
          myLists[myLists.indexOf(myLists_obj[id])] = res.data;
        else
          myLists.push(res.data);
        myLists_obj[id] = res.data;
        myListsChange();
        deferred.resolve(res.data);
      }, function () {
        deferred.reject();
      });
    });
    return deferred.promise.then(function (list) {
      list.unread_changes = 0;
      setRead(list.id);
      return list;
    });
  }

  function pin (id) {
    return $http.put('/api/lists/'+id+'/pin', {pin: true}).then(function (res) {
      myLists_obj[id].pinned = true;
      myListsChange();
      return res.data;
    });
  }

  function unpin (id) {
    return $http.put('/api/lists/'+id+'/pin', {pin: false}).then(function (res) {
      myLists_obj[id].pinned = false;
      myListsChange();
      return res.data;
    });
  }

  function setRead (id) {
    $http.put('/api/lists/'+id+'/read');
  }

  function saveList (list) {
    return $http.post('/api/lists', list).then(function (res) {
      myLists.push(res.data);
      myLists_obj[res.data.id] = res.data;
      myListsChange();
      return res.data;
    });
  }

  function deleteList (id) {
    var list = myLists_obj[id];
    var deferred = $q.defer();
    $mdDialog.show($mdDialog.confirm()
                  .title("Delete "+list.name)
                  .textContent("The cards of that list will be lost.")
                  .ariaLabel("Delete List")
                  .cancel("cancel")
                  .ok("ok")).then(function () {
      $http.delete('/api/lists/'+id).then(function (res) {
        myLists.splice(myLists.indexOf(list),1);
        delete list;
        myListsChange();
        deferred.resolve();
      });
    }, function () {
      deferred.reject();
    });
    return deferred.promise;
  }

  function createCard (listid) {
    return $translate('CARD_NEW_TITLE').then(function (card_new_title) {
      var card = {listid: listid, title: card_new_title, tags: [], content: []};
      myLists_obj[listid].cards.push(card);
      return card;
    });
  }

  function saveCards (cards) {
    return $http.put('/api/cards', {addCards: cards});
  }

  function deleteCard (card) {
    var deferred = $q.defer();
    $mdDialog.show($mdDialog.confirm()
                  .title("Delete "+card.title)
                  .textContent("The content of that card will be lost.")
                  .ariaLabel("Delete Card")
                  .cancel("cancel")
                  .ok("ok")).then(function () {

      $http.put('/api/cards', {deleteCards: [card]}).then(function () {
        deferred.resolve();
      });
    }, function () {
      deferred.reject();
    });
    return deferred.promise;
  }

  function search (query) {
    return firstLoadDeferred.promise.then(function () {
      if (query.match(/^#\S+$/)) {
        //its a tag
        return $q.when(searchTag(query.replace('#','').toLowerCase()));
      } else {
        //search in List names, card names locally; public list names on server
        var results = {lists: [], cards: [], publicLists: []};
        var regex = new RegExp(query.toLowerCase());
        myLists.forEach(function (list) {
          if (regex.test(list.name.toLowerCase()))
            results.lists.push(list);
          list.cards.forEach(function (card) {
            if (regex.test(card.title.toLowerCase()))
              results.cards.push(card);
          });
        });
        return $q.when(results);
      }
    });
  }
  function searchTag (query) {
    var results = {lists: [], cards: []};
    myLists.forEach(function (list) {
      //search tags
      list.tags.some(function (tag) {
        if (tag.toLowerCase() == query) {
          results.lists.push(list);
          return true;
        }
        return false;
      });
      //search card tags
      list.cards.forEach(function (card) {
        card.tags.some(function (tag) {
          if (tag.toLowerCase() == query) {
            results.cards.push(card);
            return true;
          }
          return false;
        });
      });
    });
    return results;
  }

  return {
    getAllLists: getAllLists,
    getList: getList,
    pin: pin,
    unpin: unpin,
    setRead: setRead,
    saveList: saveList,
    deleteList: deleteList,
    createCard: createCard,
    saveCards: saveCards,
    deleteCard: deleteCard,
    search: search,
    onMyListsChange: onMyListsChange,
    removeOnMyListsChange: removeOnMyListsChange
  }
});
