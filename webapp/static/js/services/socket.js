angular.module('colnote').factory('Socket', function (socketFactory) {
  return socketFactory();
});