var gulp = require('gulp'),
    clean = require('gulp-clean'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    spritesmith = require('gulp.spritesmith'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    uglify = require('gulp-uglify'),
    gutil = require('gulp-util'),
    ngAnnotate = require('browserify-ngannotate');

var CacheBuster = require('gulp-cachebust');
var cachebust = new CacheBuster();

/////////////////////////////////////////////////////////////////////////////////////
//
// cleans the build output
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('clean', function (cb) {
    return gulp.src('webapp/static/dist', {read: false})
    .pipe(clean());
});

/////////////////////////////////////////////////////////////////////////////////////
//
// runs sass, creates css source maps
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('build-css', ['clean'], function() {
    return gulp.src('./webapp/static/styles/*')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(cachebust.resources())
        .pipe(sourcemaps.write('./webapp/static/maps'))
        .pipe(gulp.dest('./webapp/static/dist'));
});

/////////////////////////////////////////////////////////////////////////////////////
//
// fills in the Angular template cache, to prevent loading the html templates via
// separate http requests
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('build-template-cache', ['clean'], function() {

    var ngHtml2Js = require("gulp-ng-html2js"),
        concat = require("gulp-concat");

    return gulp.src("./webapp/static/partials/**/*.html")
        .pipe(ngHtml2Js({
            moduleName: "colnotePartials",
            prefix: "/partials/"
        }))
        .pipe(concat("templateCachePartials.js"))
        .pipe(gulp.dest("./webapp/static/dist"));
});
/////////////////////////////////////////////////////////////////////////////////////
//
// Build a minified Javascript bundle - the order of the js files is determined
// by browserify
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('build-js', ['clean'], function() {
    var b = browserify({
        entries: './webapp/static/js/app.js',
        debug: true,
        paths: ['./node_modules', './webapp/static/js/'],
        transform: [ngAnnotate]
    });

    return b.bundle()
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(cachebust.resources())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(uglify())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./webapp/static/dist/js/'));
});

/////////////////////////////////////////////////////////////////////////////////////
//
// full build (except sprites), applies cache busting to the main page css and js bundles
//
/////////////////////////////////////////////////////////////////////////////////////
//online css+js
gulp.task('build-online', [ 'clean','build-css','build-template-cache','build-js'], function() {
    return gulp.src('./webapp/static/on/index.html')
        .pipe(cachebust.references())
        .pipe(gulp.dest('./webapp/static/dist'));
});
//offline css+js
gulp.task('build-offline', [ 'clean','build-css','build-template-cache','build-js'], function() {
    return gulp.src('./webapp/static/off/index.html')
        .pipe(cachebust.references())
        .pipe(gulp.dest('./webapp/static/dist'));
});

/////////////////////////////////////////////////////////////////////////////////////
//
// watches file system and triggers a build when a modification is detected
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('watch-online', function() {
    return gulp.watch(['./webapp/static/on/index.html', './webapp/static/partials/*.html', './webapp/static/partials/**/*.html', './webapp/static/styles/*.*css', './webapp/static/js/**/*.js', './webapp/static/styles/**/*.scss'], ['build-online']);
});
gulp.task('watch-offline', function() {
    return gulp.watch(['./webapp/static/off/index.html', './webapp/static/partials/*.html', './webapp/static/partials/**/*.html', './webapp/static/styles/*.*css', './webapp/static/js/**/*.js', './webapp/static/styles/**/*.scss'], ['build-offline']);
});

/////////////////////////////////////////////////////////////////////////////////////
//
// launch a build upon modification and publish it to a running server
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('dev', ['watch-online', 'build-online']);
gulp.task('dev-offline', ['watch-offline', 'build-offline']);

/////////////////////////////////////////////////////////////////////////////////////
//
// generates a sprite png and the corresponding sass sprite map.
// This is not included in the recurring development build and needs to be run separately
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('sprite', function () {

    var spriteData = gulp.src('./webapp/static/images/icons/*.png')
        .pipe(spritesmith({
            imgName: 'colnote-sprite.png',
            cssName: '_colnote-sprite.scss',
            algorithm: 'top-down',
            padding: 5
        }));

    spriteData.css.pipe(gulp.dest('./webapp/static/dist'));
    spriteData.img.pipe(gulp.dest('./webapp/static/dist'));
});

/////////////////////////////////////////////////////////////////////////////////////
//
// installs and builds everything, including sprites
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('default', ['sprite','build-online']);
