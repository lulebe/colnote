var should = require('should');

var db_proto = require('../db/db_proto.js');
var lists = require('../db/lists.js');
var errors = require('../db/db_errors.js');

describe('The lists module', function () {
  it('should have 19 functions', function () {
    Object.keys(lists).should.have.a.lengthOf(19);
    Object.keys(lists).forEach(function (fnName) {
      lists[fnName].should.be.a.Function();
    });
  });
});

describe('The getListUsers Function', function () {
  it('should return an empty array for a non-existing list', function (done) {
    lists.getListUsers({listid:0}, function (err, users) {
      users.should.be.an.instanceOf(Array);
      users.should.have.a.lengthOf(0);
      done();
    });
  });
  it('should return a non-empty array of users', function (done) {
    lists.getListUsers({listid:1}, function (err, users) {
      users.should.be.an.instanceOf(Array);
      users.length.should.not.equal(0);
      done();
    });
  });
});

describe('The getListUser Function', function () {
  it('should return permissions 0 for a non-found user', function (done) {
    lists.getListUser({listid:1, userid:500}, function (err, lu) {
      lu.permissions.should.equal(0);
      done();
    });
  });
  it('should return the correct permission', function (done) {
    lists.getListUser({listid:1, userid:1}, function (err, lu) {
      lu.permissions.should.equal(3);
      done();
    });
  });
});

describe('The changePermissions Function', function () {
  it('should allow admins to give permissions', function (done) {
    lists.changePermissions({listid:1, adminid: 1, userid:2, permissions:1}, function (err) {
      should(err).not.be.ok();
      done();
    });
  });
  it('should not allow users to give permissions', function (done) {
    lists.changePermissions({listid:1, adminid: 2, userid:1, permissions:2}, function (err) {
      err.should.equal(errors.no_permission);
      done();
    });
  });
  after(function (done) {
    db_proto.execDB(function(dbConn, args, cb) {
      dbConn.query("DELETE FROM list_users WHERE userid=2 AND listid=1", [], cb);
    }, null, done);
  });
});




describe('The getList Function', function () {
  var mList, mErr;
  before(function (done) {
    lists.getList({listid: 1}, function (err, list) {
      mList = list;
      mErr = err;
      done();
    });
  });
  it('should return an error when no list was found', function (done) {
    lists.getList({listid: 500}, function (err, list) {
      err.should.equal(errors.list_exists_not);
      done();
    });
  });
  it('should return no errors for valid lists', function () {
    should(mErr).not.be.ok();
  });
  it('should return a list object', function () {
    should(mList).be.an.instanceOf(Object);
  });
  it('should contain a valid users array', function () {
    mList.users.should.be.an.instanceOf(Array);
    mList.users.length.should.not.be.equal(0);
    mList.users.forEach(function (user) {
      user.should.have.properties(['id','name','username','permissions']);
    });
  });
  it('should contain a valid cards array', function () {
    mList.cards.should.be.an.instanceOf(Array);
    mList.cards.length.should.not.be.equal(0);
    mList.cards.forEach(function (card) {
      card.should.have.a.property('content');
      card.content.forEach(function (content) {
        content.should.have.properties(['id','type','content']);
      });
    });
  });
});


describe('The getListWithAccess Function', function () {
  it('should return an error for a user without permission', function (done) {
    lists.getListWithAccess({listid:1, userid:2}, function (err, list) {
      err.should.equal(errors.no_permission);
      should(list).not.be.ok();
      done();
    });
  });
  it('should return a list object with permissions for user w/ lID+uID', function (done) {
    lists.getListWithAccess({listid:1, userid:1}, function (err, list) {
      list.should.be.an.instanceOf(Object);
      list.should.have.a.property('permissions', 3);
      done();
    });
  });
  it('should return an error for invalid links', function (done) {
    lists.getListWithAccess({editlink: '0234674216f3e15c761ee1a5e255f067953623c8'}, function (err, list) {
      should(list).not.be.ok();
      err.should.equal(errors.link_exists_not);
      lists.getListWithAccess({viewlink: '0bcd674216f3e15c761ee1a5e255f067953623c8'}, function (err, list) {
        should(list).not.be.ok();
        err.should.equal(errors.link_exists_not);
        done();
      });
    });
  });
  it('should return a list object with permissions for user w/ viewlink', function (done) {
    lists.getListWithAccess({viewlink: 'abcd674216f3e15c761ee1a5e255f067953623c8'}, function (err, list) {
      list.should.be.an.instanceOf(Object);
      list.should.have.a.property('permissions', 1);
      done();
    });
  });
  it('should return a list object with permissions for user w/ editlink', function (done) {
    lists.getListWithAccess({editlink: '1234674216f3e15c761ee1a5e255f067953623c8'}, function (err, list) {
      list.should.be.an.instanceOf(Object);
      list.should.have.a.property('permissions', 2);
      done();
    });
  });
});


describe('The getListsForUser Function', function () {
  var mListset, mErr;
  before(function (done) {
    lists.getListsForUser({userid:1}, function (err, listset) {
      mErr=err, mListset=listset;
      done();
    });
  });
  it('should return no errors', function () {
    should(mErr).not.be.ok();
  });
  it('should return an array of lists with permissions and tags', function () {
    mListset.should.be.an.instanceOf(Array);
    mListset.forEach(function (list) {
      list.should.have.a.property('permissions');
      list.should.have.a.property('tags');
    });
  });
  it('should have a cards array with tags and content for each card on each lists', function () {
    mListset.forEach(function (list) {
      list.should.have.a.property('permissions');
      list.should.have.a.property('tags');
      list.cards.should.be.an.instanceOf(Array);
      list.cards.forEach(function (card) {
        card.should.have.a.property('tags');
        card.content.should.be.an.instanceOf(Array);
      });
    });
  });
});


describe('The getViewLink Function', function () {
  it('should return the link for admins', function (done) {
    lists.getViewLink({listid:1, userid:1}, function (err, link) {
      should(err).not.be.ok();
      link.should.be.a.String().and.have.a.lengthOf(40);
      done();
    });
  });
  it('should return an error for non-admins', function (done) {
    lists.getViewLink({listid:1, userid:2}, function (err, link) {
      err.should.equal(errors.no_permission);
      done();
    });
  });
  it('should return an error if there is no link', function (done) {
    lists.getViewLink({listid:2, userid:1}, function (err, link) {
      err.should.equal(errors.link_exists_not);
      done();
    });
  });
});


describe('The getEditLink Function', function () {
  it('should return the link for admins', function (done) {
    lists.getEditLink({listid:1, userid:1}, function (err, link) {
      should(err).not.be.ok();
      link.should.be.a.String().and.have.a.lengthOf(40);
      done();
    });
  });
  it('should return an error for non-admins', function (done) {
    lists.getEditLink({listid:1, userid:2}, function (err, link) {
      err.should.equal(errors.no_permission);
      done();
    });
  });
  it('should return an error if there is no link', function (done) {
    lists.getEditLink({listid:2, userid:1}, function (err, link) {
      err.should.equal(errors.link_exists_not);
      done();
    });
  });
});


describe('The setViewLink Function', function () {
  it('should return the link for admins', function (done) {
    lists.setViewLink({listid:2, userid:1}, function (err, link) {
      should(err).not.be.ok();
      link.should.be.a.String().and.have.a.lengthOf(40);
      done();
    });
  });
  it('should return an error for non-admins', function (done) {
    lists.setViewLink({listid:2, userid:2}, function (err, link) {
      err.should.equal(errors.no_permission);
      done();
    });
  });
  after(function (done) {
    db_proto.execDB(function (dbConn, args, cb) {
      dbConn.query("UPDATE lists SET viewlink=NULL WHERE id=2", [], cb);
    }, null, done);
  });
});


describe('The setEditLink Function', function () {
  it('should return the link for admins', function (done) {
    lists.setEditLink({listid:2, userid:1}, function (err, link) {
      should(err).not.be.ok();
      link.should.be.a.String().and.have.a.lengthOf(40);
      done();
    });
  });
  it('should return an error for non-admins', function (done) {
    lists.setEditLink({listid:2, userid:2}, function (err, link) {
      err.should.equal(errors.no_permission);
      done();
    });
  });
  after(function (done) {
    db_proto.execDB(function (dbConn, args, cb) {
      dbConn.query("UPDATE lists SET editlink=NULL WHERE id=2", [], cb);
    }, null, done);
  });
});


describe('The getListIdForLink Function', function () {
  it('should return an id for a valid viewlink', function (done) {
    lists.getListIdForLink({viewlink: 'abcd674216f3e15c761ee1a5e255f067953623c8'}, function (err, listid) {
      should(err).not.be.ok();
      listid.should.equal(1);
      done();
    });
  });
  it('should return an error for an invalid viewlink', function (done) {
    lists.getListIdForLink({viewlink: '0bcd674216f3e15c761ee1a5e255f067953623c8'}, function (err, listid) {
      err.should.equal(errors.link_exists_not);
      done();
    });
  });
  it('should return an id for a valid editlink', function (done) {
    lists.getListIdForLink({editlink: '1234674216f3e15c761ee1a5e255f067953623c8'}, function (err, listid) {
      should(err).not.be.ok();
      listid.should.equal(1);
      done();
    });
  });
  it('should return an error for an invalid editlink', function (done) {
    lists.getListIdForLink({editlink: '0234674216f3e15c761ee1a5e255f067953623c8'}, function (err, listid) {
      err.should.equal(errors.link_exists_not);
      done();
    });
  });
});


describe('The setPublic Function', function () {
  it('should set a list public and set a viewlink', function (done) {
    lists.setPublic({listid:2, userid:1}, function (err, viewlink) {
      should(err).not.be.ok();
      viewlink.should.be.a.String();
      done();
    });
  });
  it('should set a list public and keep an existing viewlink', function (done) {
    lists.setPublic({listid:1, userid:1}, function (err, viewlink) {
      should(err).not.be.ok();
      viewlink.should.equal('abcd674216f3e15c761ee1a5e255f067953623c8');
      done();
    });
  });
  it('should return an error for non-admins', function (done) {
    lists.setPublic({listid:1, userid:2}, function (err, viewlink) {
      err.should.equal(errors.no_permission);
      done();
    });
  });
  after(function (done) {
    db_proto.execDB(function(dbConn, args, cb) {
      dbConn.query("UPDATE lists SET public=0; UPDATE lists SET viewlink=NULL WHERE id=2;", [], cb);
    }, null, done);
  });
});