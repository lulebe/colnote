module.exports = function (app) {
  var User = require('../db/users.js');
  var dbErrors = require('../db/db_errors.js');
  
  function isLoggedIn (req, res, next) {
    if (req.isAuthenticated())
      next();
    else
      res.status(401).send();
  }
  
  //get own useraccount
  app.get('/api/users', isLoggedIn, function (req, res) {
    if (req.query.q) {
      if (req.query.q.length < 3) return res.status(200).send([]);
      User.search({searchquery: req.query.q}, function (err, users) {
      if (err) return res.status(500).send(err);
      if (!users || users.length == 0) return res.status(404).send();
      res.status(200).send(users);
    });
    } else
      User.getSafe({userid: req.user.id, self: true}, function (err, user) {
        if (err) return res.status(500).send(err);
        if (!user) return res.status(404).send();
        res.status(200).send(user);
      });
  });
  
  //get other account
  app.get('/api/users/:id', isLoggedIn, function (req, res) {
    User.getSafe({userid: req.params.id, self: false}, function (err, user) {
      if (err) return res.status(500).send(err);
      if (!user) return res.status(404).send();
      res.status(200).send(user);
    });
  });
  
  //---------------
  // manipulate own account
  //---------------
  
  app.post('/api/users', isLoggedIn, function (req, res) {
    req.body.id = req.user.id;
    User.change(req.body, function (err, user) {
      if (err) {
        if (err = dbErrors.user_exists_not)
          return res.status(404).send();
        return res.status(500).send();
      }
      res.status(200).send(user);
    });
  });
}