var dbErrors = require('../db/db_errors.js');
var List = require('../db/lists.js');


var io, sockets = [];

module.exports = function (sio) {
  
  if (sio) {
    io = sio;
  
    io.on('connection', function (socket) {
      sockets.push(socket);
      socket.emit('listsChange');
      socket.on('disconnect', function () {
        sockets.splice(sockets.indexOf(socket),1);
      })
    });
  }
  
  function emitForUsers (userids, event, data) {
    sockets.forEach(function (socket) {
      if (userids.indexOf(socket.request.user.id) >= 0) {
        socket.emit(event, data);
      }
    })
  };
  
  return {
    listsChange: function (userids) {
      emitForUsers(userids, 'listsChange');
    }
  };
};