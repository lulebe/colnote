module.exports = function (app) {
  var dbErrors = require('../db/db_errors.js');
  var List = require('../db/lists.js');

  function isLoggedIn (req, res, next) {
    if (req.isAuthenticated())
      next();
    else
      res.status(401).send();
  }

  function handleErrors (err, res) {
    if (err.code)
      res.status(err.code).send(err);
    else
      res.status(500).send(err);
  }

  app.get('/api/lists', function (req, res) {
    if (req.isAuthenticated() && req.query.mylists) {
      //get my lists
      List.getListsForUser({userid:req.user.id}, function (err, lists) {
        if (err) return handleErrors(err, res);
        res.status(200).send(lists);
      })
    } else {
      //get public lists
      List.getPublicLists({}, function (err, lists) {
        if (err) return handleErrors(err, res);
        res.status(200).send(lists);
      });
    }
  });

  app.get('/api/lists/:id', function (req, res) {
    var dbArgs = {listid: req.params.id};
    if (req.isAuthenticated()) dbArgs.userid = req.user.id;
    List.getListWithAccess(dbArgs, function (err, list) {
      if (err) return handleErrors(err, res);
      res.status(200).send(list);
    });
  });

  app.get('/api/viewlink/:link', function (req, res) {

  });

  app.get('/api/editlink/:link', isLoggedIn, function (req, res) {

  });

  app.post('/api/lists', isLoggedIn, function (req, res) {
    if (!req.body.name || !req.body.color) return res.status(400).send();
    var args = {
      listid: req.body.id,
      userid: req.user.id,
      name: req.body.name,
      color: req.body.color,
      public: req.body.public,
      viewlink: req.body.viewlink,
      editlink: req.body.editlink,
      users: req.body.users,
      tags: req.body.tags
    };
    List.saveList(args, function (err, list) {
      if (err) return handleErrors(err, res);
      res.status(201).send(list);
    });
  });

  app.delete('/api/lists/:id', isLoggedIn, function (req, res) {
    List.deleteList({listid: req.params.id, userid: req.user.id}, function (err) {
      if (err) return handleErrors(err, res);
      res.status(204).send();
    });
  });

  app.put('/api/lists/:id/pin', isLoggedIn, function (req, res) {
    List.pinList({listid: req.params.id, userid: req.user.id, pin: req.body.pin}, function (err, list) {
      if (err) return handleErrors(err, res);
      res.status(200).send(list);
    });
  });

  app.put('/api/lists/:id/read', isLoggedIn, function (req, res) {
    List.setRead({listid: req.params.id, userid: req.user.id}, function (err) {
      if (err) return handleErrors(err, res);
      return res.status(200).send();
    });
  });

  app.put('/api/lists/:id/tags', isLoggedIn, function (req, res) {
    List.setListTags({listid: req.params.id, userid: req.user.id, tags: req.body.tags}, function (err, list) {
      if (err) return handleErrors(err, res);
      res.status(200).send(list);
    });
  });

  app.put('/api/cards', isLoggedIn, function (req, res) {
    List.deleteCards({userid: req.user.id, cards: req.body.deleteCards}, function (err) {
      if (err) return handleErrors(err, res);
      if (!req.body.addCards) return res.status(204).send();
      List.saveCards({userid: req.user.id, cards: req.body.addCards}, function (err) {
        if (err) return handleErrors(err, res);
        res.status(201).send();
      });
    });
  });
}
