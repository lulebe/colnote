/*jslint node: true */
'use strict';


var express = require('express'),
  options = require('./config/options.js'),
  passport = require('passport'),
  flash = require('connect-flash'),
  morgan = require('morgan'),
  bodyparser = require('body-parser'),
  cookieparser = require('cookie-parser'),
  session = require('express-session'),
  mysqlstore = require('connect-mysql')({session: session}),
  socketio = require('socket.io'),
  iobundle = require('socket.io-bundle'),
  iopassport = require('socket.io-passport');

var poolPromise = require('./db/db.js');
var secrets = require('./config/secrets.json');

poolPromise.then(function (dbConnPool) {


  var app = express();

  //middleware: bodyparser and allow cross-origin requests
  app.use(bodyparser.json());
  app.use(bodyparser.urlencoded({extended: true}));
  app.use(cookieparser());
  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://127.0.0.1:9000");
    res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Accept, Encoding, Authorization");
    res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
    next();
  });
  if (process.argv[2] != '--production')
    app.use(morgan('dev'));

  app.use('/res', express.static(__dirname + '/webapp/static/res'));
  app.use('/app/dist', express.static(__dirname + '/webapp/static/dist'));
  app.set('view engine', 'hbs');
  app.set('views', __dirname + '/webapp/views');

  //passport shit
  require('./config/passport.js')(passport);
  var sessOptions = {
    secret: secrets.session,
    store: new mysqlstore({pool: dbConnPool}),
    expires: new Date(Date.now() + 3600000)
  };
  app.use(session(sessOptions));
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(flash());


  //routes
  require('./webapp/routes.js')(app, passport);

  //api
  require('./api/users.js')(app);
  require('./api/lists.js')(app);


  //start server
  var server = app.listen(options.appPort, function () {
      console.log("Server is running on " + options.appPort + "!");
    });

  //socket stuff
  var io = socketio(server);
  io.use(iobundle.cookieParser());
  io.use(iobundle.session(sessOptions));
  io.use(iopassport.initialize());
  io.use(iopassport.session());
  require('./api/push.js')(io);

});
